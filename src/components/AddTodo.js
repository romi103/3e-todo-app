import React, { useState } from "react";
import { v4 as uuid } from 'uuid';
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { addTodo } from "../store/actions/todos";
import styled from "styled-components";

const Input = styled.input.attrs({ placeholder: "New todo" })`
    background-color: ${({ theme }) => theme.color.dark};
    border-radius: 9999px;
    width: 100%;
    border: none;
    height: 5rem;
    outline: none;
    padding-left: 2rem;
    color: ${({ theme }) => theme.color.white}
`;

const Form = styled.form`
    position: relative;
    margin-bottom: 4rem;
`;

const FormButton = styled.button`
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0; 
    border: 5px solid ${({ theme }) => theme.color.dark};
    border-radius: 9999px;
    cursor: pointer;
    padding: 0 3rem;
    outline: none;
    font-size: 1.8rem;
    font-weight: 700;
    transition: all 0.3s;
    background-color: ${({ theme }) => theme.color.accent};

    &:disabled {
        background-color: #E5E5E5;
        cursor: not-allowed;
    }
`;


const AddTodo = () => {

    const [title, setTitle] = useState("");
    const isDisabled = useSelector(state => state.todos.todosList.length >= state.todos.maxItem)
    const dispatch = useDispatch();
    const handleSubmit = (e) => {
        e.preventDefault();

        if (!title) return;

        const newTodo = {
            userId: 1,
            id: uuid(),
            title: title,
            completed: false
        }

        dispatch(addTodo(newTodo));
        setTitle("");
    }

    return (
        <Form onSubmit={handleSubmit}>
            <Input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
            <FormButton disabled={isDisabled}>Add</FormButton>
        </Form>
    )
}

export default AddTodo;