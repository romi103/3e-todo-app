import React from "react";
import { useDispatch } from "react-redux";
import { toggleDone, deleteTodo } from "../store/actions/todos";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import FlexWrapper from "./utility/FlexWrapper";
import Checkbox from "./Checkbox";
import DeleteButton from "./DeleteButton";


const Title = styled.span`
    padding: 0 1rem;
    ${props => props.completed && css`
        color: #a8a8a8;
        text-decoration: line-through;
    `}
`


const Todo = ({ todo }) => {

    const { id, completed } = todo;
    const dispatch = useDispatch();

    const handleCheck = () => {
        dispatch(toggleDone(id))
    }

    const handleDelete = () => {
        dispatch(deleteTodo(id))
    }

    return (
        <FlexWrapper justify="space-between">
            <div>
                <Checkbox onChange={handleCheck} checked={todo.completed} />
                <Title completed={todo.completed}>{todo.title}</Title>
            </div>

            {completed && <DeleteButton onClick={handleDelete} />}


        </FlexWrapper>
    )
}

Todo.propTypes = {
    todo: PropTypes.object.isRequired
}

export default Todo;