import React from "react";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";

const Icon = styled.svg`
  fill: none;
  stroke: ${({ theme }) => theme.color.dark};
  cursor: pointer;
  stroke-width: 2px;
  width: 30px;
  height: 30px;
  `;



const DeleteButton = ({ ...props }) => {
  return (
    <Icon {...props}>
      <circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line>
    </Icon>
  )
}


export default DeleteButton;