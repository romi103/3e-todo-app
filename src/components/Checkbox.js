import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px`;

const Icon = styled.svg`
  fill: none;
  stroke: white;
  stroke-width: 2px`;

const StyledCheckbox = styled.div`
  display: inline-block;
  width: 24px;
  height: 24px;
  background: ${props => props.checked ? props.theme.color.accent : props.theme.color.accentLight};
  border-radius: 3px;
  transition: all 0.15s;
  cursor: pointer;
  
  ${HiddenCheckbox}:focus + & {
    box-shadow: 0 0 0 3px #f1c56f;
  }

   ${Icon} {
    visibility: ${props => props.checked ? 'visible' : 'hidden'}
  }
  `;

const CheckboxContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
`;

const Checkbox = ({ className, checked, ...props }) => {
  return (
    <CheckboxContainer className={className}>
      <label>
        <HiddenCheckbox checked={checked} {...props} />
        <StyledCheckbox checked={checked}>
          <Icon viewBox="0 0 24 24">
            <polyline points="20 6 9 17 4 12" />
          </Icon>
        </StyledCheckbox>
      </label>
    </CheckboxContainer>
  )
}

export default Checkbox;