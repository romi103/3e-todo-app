import React from 'react';
import { useDispatch } from "react-redux";
import { fetchTodos } from "../store/actions/todos";
import { ThemeProvider } from "styled-components";

import AddTodo from "../components/AddTodo";
import TodoList from "../components/TodoList";
import Container from "./utility/Container";
import theme from "../theme/theme";


const App = () => {
  const dispatch = useDispatch();
  dispatch(fetchTodos());

  return (
    <ThemeProvider theme={theme}>
      <Container>
        <AddTodo />
        <TodoList />
      </Container>
    </ThemeProvider>
  );

}

export default App;
