import React from "react";
import { useSelector } from "react-redux";
import Todo from "./Todo";
import styled from "styled-components";

const List = styled.ul`
    list-style: none;
`
const ListItem = styled.li`
    margin: 1rem 0;
    padding: 0.5rem 0;
    border-bottom: 1px solid #e1e1e1;

    &:last-child {
        border-bottom: 1px solid transparent;
    }
`

const TodoList = () => {

    const { todosList } = useSelector(state => state.todos)
    return (
        <List>
            {
                todosList.length ? todosList.map((todo) => (
                    <ListItem key={todo.id}>
                        <Todo todo={todo} />
                    </ListItem>

                )) : <ListItem><p>There is nothing to display.</p></ListItem>
            }
        </List>
    )
}

export default TodoList;