import React from "react";
import styled from "styled-components";


const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: ${props => props.justify}
`


Wrapper.defaultProps = {
    justify: "flex-start"
}


const FlexWrapper = ({ children, ...rest }) => {
    return (
        <Wrapper {...rest}>
            {children}
        </Wrapper>
    )
}

export default FlexWrapper;
