import React from "react";
import styled from 'styled-components';

const Outer = styled.div`
    width: 100vw;
    height: 100vh;
    display: grid;
    place-items: center;
    background-color: ${({ theme }) => theme.color.darker};

`

const Inner = styled.div`
  background-color: ${({ theme }) => theme.color.main};
  padding: 4rem;
      box-shadow: 0 0 8px 9px #323232;
  border-radius: 5px;
  min-width: 65%;
  
`

const Container = ({ children }) => {
    return (
        <Outer>
            <Inner>
                {children}
            </Inner>
        </Outer>
    )
}

export default Container;