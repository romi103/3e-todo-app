import axios from "axios";

const BASE_PATH = "//jsonplaceholder.typicode.com/";
const fetchTodos = async () => {
    return axios.get(`${BASE_PATH}todos`);
}

export default {
    fetchTodos
}