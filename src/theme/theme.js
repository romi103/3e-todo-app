export default {
    color: {
        accent: "#fddb3a",
        accentLight: "#fff2b6",
        main: "#f6f4e6",
        dark: "#52575d",
        darker: "#41444b",
        white: "#fff"

    }
}