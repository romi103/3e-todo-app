import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        
    }

    html {
        font-size: 62.2%;
        color: #333;
    }

    body {
        font-family: Sans-Serif; 
    }

    p, span, input {
        font-size: 1.8rem;
    }
`

export default GlobalStyle;