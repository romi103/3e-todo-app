import * as types from "../types";

export const fetchTodos = () => ({
    type: types.FETCH_TODOS
})

export const setTodos = payload => ({
    type: types.SET_TODOS,
    payload
})

export const toggleDone = id => ({
    type: types.TOGGLE_DONE,
    id
})

export const deleteTodo = id => ({
    type: types.DELETE_TODO,
    id
})

export const addTodo = todo => ({
    type: types.ADD_TODO,
    todo
})

