import * as types from "../types";
import { v4 as uuid } from 'uuid';

const initiState = {
    todosList: [],
    maxItem: 10
}

const todos = (state = initiState, action) => {
    switch (action.type) {
        case types.TOGGLE_DONE:
            return {
                ...state,
                todosList: state.todosList.map((todo) => {
                    return (todo.id !== action.id)
                        ? todo
                        : { ...todo, completed: !todo.completed }
                })
            }
        case types.DELETE_TODO:
            return {
                ...state,
                todosList: state.todosList.filter(todo => {
                    return todo.id !== action.id
                })
            }


        case types.SET_TODOS:
            return {
                ...state,
                todosList: action.payload.map(todo => {
                    return {
                        ...todo,
                        id: uuid()
                    }
                }).slice(0, state.maxItem)
            }

        case types.ADD_TODO:

            if (state.todosList.length < state.maxItem) {
                return {
                    ...state,
                    todosList: [...state.todosList, action.todo]
                }
            }
            return state

        default:
            return state;
    }
}

export default todos;