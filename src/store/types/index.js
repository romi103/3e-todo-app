export const FETCH_TODOS = "FETCH_TODOS";
export const SET_TODOS = "SET_TODOS";
export const TOGGLE_DONE = "TOGGLE_DONE";
export const DELETE_TODO = "DELETE_TODO";
export const ADD_TODO = "ADD_TODO";