import { call, put } from "redux-saga/effects";
import API from "../../service/api/api";
import * as ACTIONS from "../actions/todos";
import { takeLatest } from "redux-saga/effects";
import * as TYPES from "../types";


export function* fetchTodosSaga() {
    try {
        const response = yield call(API.fetchTodos);
        yield put(ACTIONS.setTodos(response.data));
    } catch (err) {
        console.error(err.response.data);
    }
}

export function* triggerTodosSaga() {
    yield takeLatest(TYPES.FETCH_TODOS, fetchTodosSaga);
}